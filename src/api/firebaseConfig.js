import * as firebase from 'firebase'

const config = {
    apiKey: "AIzaSyBhn-sv0hsADljlpcveRegNG1jd4OQdZQQ",
    authDomain: "outfit-app.firebaseapp.com",
    databaseURL: "https://outfit-app.firebaseio.com",
    projectId: "outfit-app",
    storageBucket: "outfit-app.appspot.com",
    messagingSenderId: "200700965422"
  }

firebase.initializeApp(config)

export const auth = firebase.auth()
export const db = firebase.database()
export const storage = firebase.storage().refFromURL('gs://outfit-app.appspot.com')
