import Vue from 'vue'
import Router from 'vue-router'
import {auth} from './api/firebaseConfig'

import Login from './views/Login.vue'
import Stylecheck from './views/Stylecheck.vue'
import Profile from './views/Profile.vue'
import Chat from './views/Chat.vue'
import Snapshot from './views/Snapshot.vue'

import Navigation from './components/Navigation.vue'

Vue.use(Router)

let router = new Router({
  routes: [
    {
      path: '*',
      redirect: '/login'
    },
    {
      path: '/',
      redirect: '/login'
    },
    {
      path: '/login',
      name: 'login',
      component: Login
    },
    {
      path: '/stylecheck',
      name: 'stylecheck',
      components: {
        default: Stylecheck,
        navigation: Navigation
      },
      meta: {
        requiresAuth: true
      }
    },
    {
      path: '/profile',
      name: 'profile',
      components: {
        default: Profile,
        navigation: Navigation
      },
      meta: {
        requiresAuth: true
      }
    },
    {
      path: '/chat',
      name: 'chat',
      components: {
        default: Chat,
        navigation: Navigation
      },
      meta: {
        requiresAuth: true
      }
    },
    {
      path: '/snapshot',
      name: 'snapshot',
      components: {
        default: Snapshot,
        navigation: Navigation
      },
      meta: {
        requiresAuth: true
      }
    },
  ]
})


router.beforeEach((to, from, next) => {
  let currentUser = auth.currentUser
  let requiresAuth = to.matched.some(record => record.meta.requiresAuth)

  if (requiresAuth && !currentUser) next('login')
  else next()
})

export default router
