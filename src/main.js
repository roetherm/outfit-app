import Vue from 'vue'
import './plugins/vuetify'
import Vuetify from 'vuetify'
import App from './App.vue'
import router from './router'
import {store} from './store'
import Promise from 'es6-promise-promise'
import Vuex from 'vuex'

Vue.use(Vuetify, {
  theme: {
    primary: '#242F40',
    secondary: '#CCA43B',
    accent: '#CCA43B'
  }
})

Vue.config.productionTip = false

Promise.polyfill()

Vue.use(Vuex)

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')
