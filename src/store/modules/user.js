import * as types from '../mutation-types'
import {db} from '../../api/firebaseConfig'

const state = {
  user: {},
  userId: ''
}

const getters = {
  getUser: state => state.user,
  getUserId: state => state.userId
}

const actions = {
  setUser ({commit}, user) {
    commit(types.SET_USER_ID, user.user.uid)
    return new Promise(function (resolve) {
      db.ref('users/' + user.user.uid).on('value', (userdata) => {
        let user = userdata.val()
        commit(types.SET_USER, user)
        resolve()
      })
    })
  },
  logoutUser ({commit}) {
    commit(types.DELETE_USER)
  }
}

const mutations = {
  [types.SET_USER_ID] (state, userId) {
    state.userId = userId
  },
  [types.SET_USER] (state, user) {
    state.user = user
  },
  [types.DELETE_USER] (state) {
    state.user = {}
  }
}

export default {
  state,
  getters,
  actions,
  mutations
}
